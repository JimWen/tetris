#ifndef DRAWANDSHOW_H_H_H
#define DRAWANDSHOW_H_H_H

void DrawSquare(const HDC hdc, const  unsigned int xPos, const  unsigned int yPos, const unsigned int uiColorIndex);
void DrawSquaresArray(const HDC hdc, const  unsigned int xPos, const  unsigned int yPos, 
				 unsigned int uiSquareArray[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH]);
void DrawNextWindow(const HDC hdc, unsigned int uiShapeIndex);
void DrawCurrentSquare(const HDC hdc,  int xIndex,  int yIndex,unsigned int uiCurrentShapeIndex,unsigned int uiCurDirectionIndex);
void DrawWorkWindow(const HDC hdc);
void ShowCurrentState(const HDC hdc);
void RefreshWorkWindow(HWND hwnd);
#endif
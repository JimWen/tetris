#include <Windows.h>
#include "resource.h"
#include "Variable.h"
#include "DrawAndShow.h"
#include "OperateAndEstimate.h"

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	LPCREATESTRUCT pcs;
	HWND hwndSoundBtn, hwndPauseBtn, hwndQuitBtn;
	unsigned int (*uiTempShape)[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
	int i,j;
	unsigned int uiEliminateLines;
	HMENU hMenu;

	switch (message)
	{
	case WM_CREATE:

			//定时器种子
			srand(time(NULL)); 

		//获得创建结构体以获得当前程序句柄
		pcs = (LPCREATESTRUCT)(lParam);

		//创建声音开关、暂停和退出按钮
		hwndSoundBtn = CreateWindow(TEXT("button"),
																	TEXT("声音"),
																	BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
																	2*MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1)+NEXT_WINDOW_WIDTH*SQUARE_SIZE/2,
																	MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_HIGHT*SQUARE_SIZE-(WORK_WINDOW_HIGHT-1)-5*BUTTON_HIGHT,
																	BUTTON_WIDTH,
																	BUTTON_HIGHT,
																	hwnd, 
																	(HMENU)IDB_SOUND,
																	pcs->hInstance,
																	NULL
																	);
		hwndPauseBtn =  CreateWindow(TEXT("button"),
																	TEXT("开始/暂停"),
																	BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
																	2*MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1)+NEXT_WINDOW_WIDTH*SQUARE_SIZE/2,
																	MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_HIGHT*SQUARE_SIZE-(WORK_WINDOW_HIGHT-1)-3*BUTTON_HIGHT,
																	BUTTON_WIDTH,
																	BUTTON_HIGHT,
																	hwnd, 
																	(HMENU)IDB_PAUSE,
																	pcs->hInstance,
																	NULL
																	);
		hwndQuitBtn =		CreateWindow(TEXT("button"),
																	TEXT("退出"),
																	BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
																	2*MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1)+NEXT_WINDOW_WIDTH*SQUARE_SIZE/2,
																	MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_HIGHT*SQUARE_SIZE-(WORK_WINDOW_HIGHT-1)-BUTTON_HIGHT,
																	BUTTON_WIDTH,
																	BUTTON_HIGHT,
																	hwnd, 
																	(HMENU)IDB_QUIT,
																	pcs->hInstance,
																	NULL
			);

		//环境初始化
		ResetAll();

		hMenu = GetMenu(hwnd);
		//确认开始时音效打开
		CheckMenuItem(GetSubMenu(hMenu,0),1,(bIsAudioOpen ? MF_CHECKED : MF_UNCHECKED) | MF_BYPOSITION);
		DeleteObject(hMenu);
		return (0);

	case WM_PAINT:
		hdc = BeginPaint (hwnd, &ps);

		DrawWorkWindow(hdc);
		DrawCurrentSquare(hdc,iCurrentXIndex,iCurrentYIndex,uiCurrentShapeIndex,uiCurDirectionIndex);
		DrawNextWindow(hdc,uiNextShapeIndex);
		ShowCurrentState(hdc);

		EndPaint (hwnd, &ps);
		return (0);

	case WM_COMMAND:
		hMenu = GetMenu(hwnd);

		switch(LOWORD(wParam))
		{
		    case IDM_START:
			case IDB_PAUSE:
			if (FALSE == bISGameStart)
			{
				SetTimer(hwnd,TIMER_1,NORMAL_TIME_ELAPSE,NULL);
				bISGameStart = TRUE;

				//开始打对勾
				CheckMenuItem(GetSubMenu(hMenu,0),0,MF_CHECKED | MF_BYPOSITION);
			}
			else
			{
				KillTimer(hwnd,TIMER_1);
				bISGameStart = FALSE;

				//取消打对勾
				CheckMenuItem(GetSubMenu(hMenu,0),0,MF_UNCHECKED | MF_BYPOSITION);
			}
			break;

			case IDM_QUIT:
			case IDB_QUIT:
				SendMessage(hwnd,WM_DESTROY,0,0);
				SetFocus(hwnd);
			break;

			case IDM_SOUND:
			case IDB_SOUND:
				bIsAudioOpen = !bIsAudioOpen;
				CheckMenuItem(GetSubMenu(hMenu,0),1,(bIsAudioOpen ? MF_CHECKED : MF_UNCHECKED) | MF_BYPOSITION);
				SetFocus(hwnd);
			break;
		}

		DeleteObject(hMenu);
		break;

	case WM_KEYDOWN:
		switch(wParam)
		{
		case VK_UP:
			if (TRUE == bISGameStart)
			{
				ChangeCurDirection();
				RefreshWorkWindow(hwnd);
			}
			break;
		case VK_LEFT:
			if (TRUE == bISGameStart)
			{
				LeftMove();
				RefreshWorkWindow(hwnd);
			}
			break;
		case VK_RIGHT:
			if (bISGameStart == TRUE)
			{
				RightMove();
				RefreshWorkWindow(hwnd);
			}
			break;
		case VK_DOWN:
			if (TRUE==bISGameStart)
			{
				SetMoveSpeed(hwnd, FALSE);
				RefreshWorkWindow(hwnd);
			}
			break;
		case VK_SPACE:
			SendMessage(hwnd,WM_COMMAND,MAKEWPARAM(IDM_START,0),0);
			break;
		}
		break;

	case WM_KEYUP:
		switch (wParam)
		{
		case VK_DOWN:
			if (bISGameStart == TRUE)
			{
				SetMoveSpeed(hwnd, TRUE);
				RefreshWorkWindow(hwnd);
			}
			break;
		}
		break;
		break;

	case WM_TIMER:
		if (FALSE == DownMove())
		{
			//获取当前的方块
			switch (uiCurrentShapeIndex)
			{
			case 0:
				uiTempShape = &uiIShape[uiCurDirectionIndex];
				break;
			case 1:
				uiTempShape = &uiTShape[uiCurDirectionIndex];
				break;
			case 2:
				uiTempShape =& uiOShape[uiCurDirectionIndex];
				break;
			case 3:
				uiTempShape = &uiSShape[uiCurDirectionIndex];
				break;
			case 4:
				uiTempShape = &uiZShape[uiCurDirectionIndex];
				break;
			case 5:
				uiTempShape = &uiLShape[uiCurDirectionIndex];
				break;
			case 6:
				uiTempShape = &uiJShape[uiCurDirectionIndex];
				break;
			}

			//将当前小方块赋给工作窗口
			for (i=0;i<NEXT_WINDOW_HIGHT;i++)
			{
				for (j=0;j<NEXT_WINDOW_WIDTH;j++)
				{
					if ((*uiTempShape)[i][j]!=0)
					{
						uiWorkWindow[i+iCurrentYIndex][j+iCurrentXIndex+1]=(*uiTempShape)[i][j];
					}
				}
			}

			//判断游戏是否结束
			if (IsGameOver() == TRUE)
			{
				if (bISGameStart == TRUE)
				{
					KillTimer(hwnd,TIMER_1);
					bISGameStart = FALSE;
				}
				MessageBox(hwnd, TEXT("当前游戏已经结束"),TEXT("警告"),MB_OK);
			}
			else
			{
				//判断消去情况和消去必要行数，更新得分和消去行数
				hdc = GetDC(hwnd);
				if ((uiEliminateLines = EliminateSquares()) > 0)
				{
					AddSound(TEXT(".\\sound\\eliminate.wav"));
					CalcState(uiEliminateLines);
					ShowCurrentState(hdc);
				}

				//当前小方块设置
				uiCurrentShapeIndex=uiNextShapeIndex;
				uiCurDirectionIndex=0;
				iCurrentXIndex=(WORK_WINDOW_WIDTH-NEXT_WINDOW_WIDTH)/2;
				iCurrentYIndex=GetStartPosY(uiCurrentShapeIndex);

				//刷新显示当前窗口
				RefreshWorkWindow(hwnd);

				//产生和显示下一个方块
				uiNextShapeIndex = GenerateNextShape();
				DrawNextWindow(hdc,uiNextShapeIndex);
				ReleaseDC(hwnd,hdc);
			}
		}
		else
		{
			RefreshWorkWindow(hwnd);
		}

		return 0;

	case WM_DESTROY:
		if (TRUE == bISGameStart)
		{
			KillTimer(hwnd,TIMER_1);
		}
		PostQuitMessage (0);
		return (0);
	}
	return DefWindowProc (hwnd, message, wParam, lParam);
}
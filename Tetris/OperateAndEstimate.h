#ifndef OPERATEANDESTIMATE_H_H_H
#define OPERATEANDESTIMATE_H_H_H

#pragma comment(lib,"winmm.lib")

unsigned int GenerateNextShape();
void ChangeCurDirection();
BOOL LeftMove();
BOOL RightMove();
BOOL DownMove();
void SetMoveSpeed(HWND hwnd, BOOL bIsSetNormalSpeed);
void ResetAll();
unsigned int EliminateSquares();
void CalcState(unsigned int uiEliminateLines);
int GetStartPosY(unsigned int uiShapeIndex);
BOOL IsGameOver();
void AddSound(PCSTR szSoundName);

#endif
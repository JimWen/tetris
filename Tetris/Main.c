#include <Windows.h>
#include "WndProc.h"
#include "resource.h"
#include "Variable.h"

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
    static TCHAR		szAppName[] = TEXT ("Tetris");
    HWND					hwnd;
    MSG						msg;
	WNDCLASSEX   wndclassex = {0};

	//窗口类设计
    wndclassex.cbSize					= sizeof(WNDCLASSEX);
    wndclassex.style						= CS_HREDRAW | CS_VREDRAW;
    wndclassex.lpfnWndProc		= WndProc;
    wndclassex.cbClsExtra			= 0;
    wndclassex.cbWndExtra			= 0;
    wndclassex.hInstance				= hInstance;
    wndclassex.hIcon						= LoadIcon (hInstance, MAKEINTRESOURCE(IDI_APPICON));//采用自定义图标
    wndclassex.hCursor					= LoadCursor (NULL, IDC_ARROW);
    wndclassex.hbrBackground	= (HBRUSH) GetStockObject (WHITE_BRUSH);
    wndclassex.lpszMenuName	= MAKEINTRESOURCE(IDR_MAINMENU);
    wndclassex.lpszClassName	= szAppName;
    wndclassex.hIconSm				= wndclassex.hIcon;
	
	//注册窗口类
    if (!RegisterClassEx (&wndclassex))
    {
        MessageBox (NULL, TEXT ("RegisterClassEx failed!"), szAppName, MB_ICONERROR);
        return 0;
    }

	//创建窗口
    hwnd = CreateWindowEx (WS_EX_OVERLAPPEDWINDOW , 
		                  szAppName, 
        		          TEXT ("俄罗斯方块V1"),
						  WS_OVERLAPPEDWINDOW & ~WS_THICKFRAME & ~WS_MAXIMIZEBOX,//普通窗口上去掉动态调整大小和最大化功能
		                  CW_USEDEFAULT, 
        		          CW_USEDEFAULT, 
                		  2*GetSystemMetrics(SM_CXBORDER)+3*MYWINDOW_SPACE+4*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1)+NEXT_WINDOW_WIDTH*SQUARE_SIZE-(NEXT_WINDOW_WIDTH-1), 
						  3*GetSystemMetrics(SM_CYBORDER)+GetSystemMetrics(SM_CYCAPTION)+2*MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_HIGHT*SQUARE_SIZE-(WORK_WINDOW_HIGHT-1)+GetSystemMetrics(SM_CYMENU), 
        		          NULL, 
                		  NULL, 
		                  hInstance,
        		          NULL); 
		
	//及时更新显示窗口
    ShowWindow (hwnd, iCmdShow);
    UpdateWindow (hwnd);
	
	//消息循环
    while (GetMessage (&msg, NULL, 0, 0))
    {
        TranslateMessage (&msg);
        DispatchMessage (&msg);
    }

    return msg.wParam;
}
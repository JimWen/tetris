//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Tetris.rc
//
#define IDI_APPICON                     101
#define IDB_SOUND                       102
#define IDR_MENU1                       102
#define IDR_MAINMENU                    102
#define IDB_PAUSE                       103
#define IDB_QUIT                        104
#define IDM_START                       40001
#define ID_40002                        40002
#define ID_40003                        40003
#define IDM_QUIT                        40004
#define IDM_SOUND                       40005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
